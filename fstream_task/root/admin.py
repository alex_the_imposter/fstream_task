from django.contrib import admin

from . import models


class MsgToAdmin(admin.ModelAdmin):

    fields = ['text']

    list_display = ['sender', 'date_creation', 'email', 'status']


admin.site.register(models.MsgToAdmin, MsgToAdmin)
