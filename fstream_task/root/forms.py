
from django_registration.forms import RegistrationFormUniqueEmail

from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django.forms import ModelForm
from django.forms import widgets
from django import forms
from django.conf import settings


from .models import MsgToAdmin


class UserRegistrationForm(RegistrationFormUniqueEmail):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for fieldname in ['username', 'password1', 'password2', 'email']:

            self.fields[fieldname].help_text = None
            self.fields[fieldname].widget.attrs = {'class': 'form-control'}


class UserAuthenticationForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs = {'class': 'form-control'}


class MsgToAdminForm(ModelForm):

    class Meta:
        model = MsgToAdmin
        fields = ('email', 'text')
        widgets = {
            'text': widgets.Textarea(attrs={'class': 'form-control', 'minlength': '20'}),
            'email': widgets.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'email': 'Choose admin email',
            'text': 'Write the message',
        }
