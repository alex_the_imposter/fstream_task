from django.urls import path, include
from . import views


app_name = 'root'

urlpatterns = [
    path('', views.UserLoginView.as_view(), name='user_login'),
    path('logout/', views.UserLogoutView.as_view(), name='user_logout'),
    path('register/', views.UserRegistrationView.as_view(), name='user_registration'),
    path('message/', views.MsgToAdminView.as_view(), name='user_email_to_admin'),
]