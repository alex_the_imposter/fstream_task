import smtplib

from django.core.mail import send_mail
from django.contrib.auth.views import FormView, LoginView, LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login

from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView

from .forms import UserRegistrationForm, UserAuthenticationForm, MsgToAdminForm
from .models import MsgToAdmin


class UserRegistrationView(CreateView):

    form_class = UserRegistrationForm
    template_name = 'root/registration.html'
    success_url = reverse_lazy('root:user_email_to_admin')

    def form_valid(self, form):
        
        to_the_endpoint = super().form_valid(form)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password1']
        registered_user = authenticate(username=username, password=password)
        login(self.request, registered_user)
        
        return to_the_endpoint

    def form_invalid(self, form):

        response = {
            'errors': form.errors
        }
    
        return JsonResponse(response)


class UserLoginView(LoginView):

    form_class = UserAuthenticationForm
    template_name = 'root/login.html'
    extra_context = {
        'next': reverse_lazy('root:user_email_to_admin')
    }

    def form_valid(self, form):
        super().form_valid(form)

        response = {
            'success_login_redirect': form.data['next']
        }

        return JsonResponse(response)

    def form_invalid(self, form):

        response = {
            'errors': form.errors
        }

        return JsonResponse(response)


class UserLogoutView(LogoutView):
    next_page = '/'


class MsgToAdminView(LoginRequiredMixin, FormView):

    form_class = MsgToAdminForm
    template_name = 'root/message.html'
    success_url = reverse_lazy('root:user_email_to_admin')
    login_url = reverse_lazy('root:user_login')

    def form_valid(self, form):

        message = form.cleaned_data['text']
        sender = self.request.user
        sender_email = sender.email
        recipient = form.cleaned_data['email']

        try:
            result = 0
            result = send_mail(
                subject='Subject heading',
                message=message,
                from_email=sender_email,
                recipient_list=[recipient]
            )
        except smtplib.SMTPException as e:

            print('*'*15)
            print('the error is... ' + str(e))
            print('*'*15)

        finally:
            unsent = 0
            sent = 1

            status = sent if result == 1 else unsent

            c = MsgToAdmin(
                sender=sender,
                email=recipient,
                text=message,
                status=MsgToAdmin.STATUS[status][0]
            )
            c.save()

        response = {
            'msg': 'Message was successfully sent'
        }

        return JsonResponse(response)

    def form_invalid(self, form):

        response = {
            'errors': form.errors
        }

        return JsonResponse(response)
