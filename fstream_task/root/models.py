from django.db import models
from django.contrib.auth import get_user_model
from django.conf import settings


class MsgToAdmin(models.Model):
    STATUS = (
        (0, 'Unsent'),
        (1, 'Sent')
    )

    sender = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='messages')
    email = models.EmailField(choices=settings.ADMINS, verbose_name='To admin email')
    text = models.TextField(max_length=600)
    status = models.SmallIntegerField(choices=STATUS, default=STATUS[0][0])
    date_creation = models.DateTimeField('Creation date', auto_now_add=True)

    class Meta:
        verbose_name = 'Message to admin'
        verbose_name_plural = 'Messages to admins'
        ordering = ('id',)

    def __str__(self):
        return 'Message details'


