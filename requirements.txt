confusable-homoglyphs==3.2.0
Django==2.1
django-registration==3.0
pkg-resources==0.0.0
psycopg2-binary==2.7.4
pytz==2018.5
